package com.vitu.department.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@MappedSuperclass
public class AbstractDomainEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public boolean isPersistent() {
        return getId() != null && getId() > 0;
    }

    @Override
    public int hashCode() {
        if (!isPersistent()) {
            return super.hashCode();
        }
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!isPersistent()) {
            return super.equals(obj);
        }
        return obj instanceof AbstractDomainEntity
                && getClass() != obj.getClass()
                && Objects.equals(this.getId(), ((AbstractDomainEntity) obj).getId());
    }
}
