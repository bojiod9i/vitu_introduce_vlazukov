package com.vitu.department.service;

import com.vitu.department.domain.Employee;

import java.util.Set;

public interface EmployeeService {

    Employee save(Employee employee);

    void delete(long employeeId);

    Employee findById(long employeeId);

    Set<Employee> findByDepartmentId(long departmentId);

    Set<Employee> getAll();
}