package com.vitu.department.service.impl;

import com.vitu.department.domain.Department;
import com.vitu.department.domain.Employee;
import com.vitu.department.exception.EntityNotFoundException;
import com.vitu.department.repository.DepartmentRepository;
import com.vitu.department.repository.EmployeeRepository;
import com.vitu.department.service.EmployeeService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private final DepartmentRepository departmentRepository;
    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(DepartmentRepository departmentRepository, EmployeeRepository employeeRepository) {
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public void delete(long employeeId) {
        Employee employee = employeeRepository.findById(employeeId);
        if (employee != null) {
            employeeRepository.delete(employee);
        } else {
            throw new EntityNotFoundException("Employee doesn't exist");
        }
    }

    @Override
    public Employee findById(long employeeId) {
        Employee employee = employeeRepository.findById(employeeId);
        if (employee == null) {
            throw new EntityNotFoundException("Employee doesn't exist");
        }
        return employee;
    }

    @Override
    public Set<Employee> findByDepartmentId(long departmentId) {
        Department department = departmentRepository.findById(departmentId);
        if (department == null) {
            throw new EntityNotFoundException("Department doesn't exist");
        }
        return department.getEmployees();
    }

    @Override
    public Set<Employee> getAll() {
        return employeeRepository.entitySet();
    }
}
