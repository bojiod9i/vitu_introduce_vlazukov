package com.vitu.department.service.impl;

import com.vitu.department.domain.Department;
import com.vitu.department.domain.Employee;
import com.vitu.department.exception.BusinessException;
import com.vitu.department.exception.EntityNotFoundException;
import com.vitu.department.repository.DepartmentRepository;
import com.vitu.department.repository.EmployeeRepository;
import com.vitu.department.service.DepartmentService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;
import java.util.Set;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    private final EmployeeRepository employeeRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository, EmployeeRepository employeeRepository) {
        this.departmentRepository = departmentRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public void assignEmployee(Long departmentId, Long employeeId) {
        Objects.requireNonNull(departmentId, "Department must not be null");
        Objects.requireNonNull(employeeId, "Employee must not be null");
        Employee employee = employeeRepository.findById(employeeId);
        if (employee == null) {
            throw new EntityNotFoundException("Employee doesn't exist");
        }
        if (employee.getDepartment() != null) {
            if (employee.getDepartment().getId().equals(departmentId)) {
                throw new BusinessException("Employee has already been assigned to department");
            } else {
                throw new BusinessException("Employee has already been assigned to another department");
            }
        }
        Department department = departmentRepository.findById(departmentId);
        if (department == null) {
            throw new BusinessException("Department doesn't exist");
        }
        employee.setDepartment(department);
        employeeRepository.save(employee);
    }

    @Override
    public void rejectEmployee(Long departmentId, Long employeeId) {
        Objects.requireNonNull(departmentId, "Department must not be null");
        Objects.requireNonNull(employeeId, "Employee must not be null");
        Employee employee = employeeRepository.findById(employeeId);
        if (employee.getDepartment() == null) {
            throw new BusinessException("Employee hasn't been assigned to department");
        } else if (!employee.getDepartment().getId().equals(departmentId)) {
            throw new BusinessException("Employee has been assigned to another department");
        }
        employee.setDepartment(null);
        employeeRepository.save(employee);
    }

    @Override
    public Set<Department> getAll() {
        return departmentRepository.entitySet();
    }
}
