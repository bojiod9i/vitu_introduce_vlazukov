package com.vitu.department.service;

import com.vitu.department.domain.Department;

import java.util.Set;

public interface DepartmentService {

    void assignEmployee(Long departmentId, Long employeeId);

    void rejectEmployee(Long departmentId, Long employeeId);

    Set<Department> getAll();
}
