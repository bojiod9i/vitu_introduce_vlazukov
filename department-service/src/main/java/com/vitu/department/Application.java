package com.vitu.department;

import com.vitu.department.service.EmployeeService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;

@ComponentScan(basePackages = "com.vitu.department")
public class Application {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
        EmployeeService employeeService = context.getBean(EmployeeService.class);
        employeeService.getAll().forEach(employee -> LOGGER.info(employee.getFirstName() + ' ' + employee.getLastName()));

    }
}