package com.vitu.department.repository;

import com.vitu.department.domain.AbstractDomainEntity;

import java.util.Set;

public interface GenericRepository<T extends AbstractDomainEntity> {

    T findById(Long id);

    Set<T> entitySet();

    T save(T entity);

    void delete(T entity);
}
