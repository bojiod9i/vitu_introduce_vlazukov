package com.vitu.department.repository.impl;

import com.vitu.department.domain.Department;
import com.vitu.department.repository.DepartmentRepository;
import org.springframework.stereotype.Repository;

@Repository
public class DepartmentRepositoryImpl extends GenericRepositoryImpl<Department> implements DepartmentRepository {

}
