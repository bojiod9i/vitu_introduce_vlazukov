package com.vitu.department.repository.impl;

import com.vitu.department.domain.Employee;
import com.vitu.department.repository.EmployeeRepository;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeRepositoryImpl extends GenericRepositoryImpl<Employee> implements EmployeeRepository {
}
