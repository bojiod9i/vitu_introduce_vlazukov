package com.vitu.department.repository;

import com.vitu.department.domain.Employee;

public interface EmployeeRepository extends GenericRepository<Employee> {
}
