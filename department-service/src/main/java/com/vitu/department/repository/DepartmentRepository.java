package com.vitu.department.repository;

import com.vitu.department.domain.Department;

public interface DepartmentRepository extends GenericRepository<Department> {
}
