package com.vitu.department.repository.impl;

import com.vitu.department.domain.AbstractDomainEntity;
import com.vitu.department.repository.GenericRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

public abstract class GenericRepositoryImpl<T extends AbstractDomainEntity> implements GenericRepository<T> {

    protected EntityManager entityManager;

    private Class<T> type;

    public GenericRepositoryImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }

    @Override
    public T findById(Long id) {
        return entityManager.find(type, id);
    }

    @Override
    public Set<T> entitySet() {
        String s = "FROM " + type.getSimpleName();
        Query query = entityManager.createQuery(s);
        return new HashSet<T>(query.getResultList());
    }

    @Override
    public T save(T entity) {
        if (entity.getId() == null) {
            entityManager.persist(entity);
            return entity;
        } else {
            return entityManager.merge(entity);
        }
    }

    @Override
    public void delete(T entity) {
        entityManager.remove(entity);
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}

