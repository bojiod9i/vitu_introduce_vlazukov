INSERT INTO 
	department (id, name)
VALUES
  	(1, 'dmvdesk'),
  	(2, 'vitu');

INSERT INTO
    employee (department_id, firstName, lastName, phone)
VALUES
  	(1, 'Vladimir', 'Lazukov', '+79062656993'),
  	(1, 'Alex', 'Morozov', '+7911 000 00 00'),
  	(1, 'Vasiliy', 'Erokhin', '+7911 000 00 01'),
  	(2, 'Alex', 'Varkulevich', '+7911 000 00 02'),
  	(2, 'Alexey', 'Larin', '+7911 000 00 03');