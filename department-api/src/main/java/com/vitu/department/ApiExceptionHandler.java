package com.vitu.department;

import com.vitu.department.exception.BusinessException;
import com.vitu.department.exception.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ApiExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleException(RuntimeException e) {
        LOGGER.error("Internal server error", e);
        return e.getMessage();
    }

    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String handleException(BusinessException e) {
        LOGGER.warn("Business exception was thrown", e);
        return e.getMessage();
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public List<String> handleException(MethodArgumentNotValidException exception) {
        LOGGER.warn("Api illegal argument", exception);
        return exception.getBindingResult().getFieldErrors().stream()
                .map(e -> "Field error in object " + e.getObjectName() + " on field " + e.getField() + " : rejected value " + e.getRejectedValue())
                .collect(Collectors.toList());
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String handleException(EntityNotFoundException exception) {
        LOGGER.warn("EntityNotFoundException exception was thrown", exception);
        return exception.getMessage();
    }
}
