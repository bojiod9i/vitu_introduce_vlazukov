package com.vitu.department.converter;

import com.vitu.department.domain.Employee;
import com.vitu.department.dto.EmployeeRequestDto;
import com.vitu.department.dto.EmployeeResponseDto;

public class EmployeeModelDtoConverter {

    private EmployeeModelDtoConverter() {
    }

    public static EmployeeResponseDto modelToResponseDto(Employee model) {
        EmployeeResponseDto dto = new EmployeeResponseDto();
        dto.setId(model.getId());
        dto.setFirstName(model.getFirstName());
        dto.setLastName(model.getLastName());
        dto.setPhone(model.getPhone());
        dto.setDepartmentId(model.getDepartment() != null ? model.getDepartment().getId() : null);
        return dto;
    }

    public static Employee dtoToModel(EmployeeRequestDto dto) {
        Employee model = new Employee();
        dtoToModel(dto, model);
        return model;
    }

    public static Employee dtoToModel(EmployeeRequestDto dto, Employee model) {
        model.setId(dto.getId());
        model.setFirstName(dto.getFirstName());
        model.setLastName(dto.getLastName());
        model.setPhone(dto.getPhone());
        return model;
    }

}
