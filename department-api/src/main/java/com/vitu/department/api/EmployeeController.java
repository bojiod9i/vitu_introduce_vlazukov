package com.vitu.department.api;

import com.vitu.department.converter.EmployeeModelDtoConverter;
import com.vitu.department.domain.Employee;
import com.vitu.department.dto.EmployeeRequestDto;
import com.vitu.department.dto.EmployeeResponseDto;
import com.vitu.department.exception.BusinessException;
import com.vitu.department.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employee")
@Api(tags = "Employee Service")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @ApiOperation("Load employee by id")
    @GetMapping("/{employeeId}")
    public EmployeeResponseDto getEmployee(@PathVariable long employeeId) throws BusinessException {
        return EmployeeModelDtoConverter.modelToResponseDto(employeeService.findById(employeeId));
    }

    @ApiOperation("Add\\Edit employee to system")
    @PutMapping("/")
    public void createOrUpdateEmployee(@Valid @RequestBody EmployeeRequestDto dto) {
        Employee employee;
        if (dto.getId() != null) {
            Employee byId = employeeService.findById(dto.getId());
            employee = EmployeeModelDtoConverter.dtoToModel(dto, byId);
        } else {
            employee = EmployeeModelDtoConverter.dtoToModel(dto);
        }
        employeeService.save(employee);
    }

    @ApiOperation("Delete employee from system")
    @DeleteMapping("/{employeeId}")
    public void deleteEmployee(@PathVariable int employeeId) {
        employeeService.delete(employeeId);
    }

    @ApiOperation("Load list of employees")
    @GetMapping("/list")
    public List<EmployeeResponseDto> getEmployees() throws BusinessException {
        return employeeService.getAll().stream().map(EmployeeModelDtoConverter::modelToResponseDto).collect(Collectors.toList());
    }

}
