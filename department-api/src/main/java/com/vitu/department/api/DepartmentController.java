package com.vitu.department.api;

import com.vitu.department.converter.EmployeeModelDtoConverter;
import com.vitu.department.domain.Department;
import com.vitu.department.dto.EmployeeResponseDto;
import com.vitu.department.exception.BusinessException;
import com.vitu.department.service.DepartmentService;
import com.vitu.department.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/department")
@Api(tags = "Department Service")
public class DepartmentController {

    private final DepartmentService departmentService;
    private final EmployeeService employeeService;

    @Autowired
    public DepartmentController(DepartmentService departmentService, EmployeeService employeeService) {
        this.departmentService = departmentService;
        this.employeeService = employeeService;
    }

    @ApiOperation("Load list of departments")
    @GetMapping("/list")
    public List<Department> getDepartments() throws BusinessException {
        return new ArrayList<>(departmentService.getAll());
    }

    @ApiOperation("Load list of employees by department")
    @GetMapping("/{departmentId}/employees")
    public List<EmployeeResponseDto> getEmployee(@PathVariable long departmentId) throws BusinessException {
        return employeeService.findByDepartmentId(departmentId).stream().map(EmployeeModelDtoConverter::modelToResponseDto).collect(Collectors.toList());
    }

    @ApiOperation("Assign employee to department")
    @PostMapping("/{departmentId}/assign/employee")
    public void assignEmployee(@PathVariable long departmentId, @NotNull @RequestParam long employeeId) throws BusinessException {
        departmentService.assignEmployee(departmentId, employeeId);
    }

    @ApiOperation("Reject employee from department")
    @PostMapping("/{departmentId}/reject/employee")
    public void rejectEmployee(@PathVariable long departmentId, @NotNull @RequestParam long employeeId) throws BusinessException {
        departmentService.rejectEmployee(departmentId, employeeId);
    }
}
